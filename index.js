class Linkedlist {
  constructor(node){
    this.head = {
      value: node,
      next: null
    };
    this.length=1;
  }

  addToHead(node){
    const newNode = {value: node, next: this.head};
    this.head = newNode;
    this.length++;
    return this;
  }

  addToList(node){
    const newNode = {value: node, next: null};
    let prevNode = this.head;
    let nextNode = prevNode.next;
    while(nextNode !== null){
      prevNode = nextNode;
      nextNode = nextNode.next;
    }
    prevNode.next = newNode;
    this.length++;
  }

  removeFromHead(){
    if(this.length === 0){
      console.log("Operation is not supported.");
    }
    else{
      const val = this.head.value;
      this.head = this.head.next;
      this.length--;
      console.log("Value "+val+" is deleted");
    }
  }

  removeANode(node){
    if(this.length === 0){
      console.log("Operation is not supported.");
    }
    else if(this.head.value === node){
      this.removeFromHead();
    }
    else{
      let prevNode = this.head;
      let nextNode = prevNode.next;
      // The first case is covered by above else if condition
      while(nextNode !== null){
        if(nextNode.value === node){
          break;
        }
        prevNode = nextNode;
        nextNode = nextNode.next;
      }
      prevNode.next = nextNode.next;
      this.length--;
    }
  }

  findANode(node){
    let count =1; //as we're starting at next element
    let found = 0;
    if(this.length === 0){
      console.log("List is empty");
    }
    else if(this.head.value === node){
      console.log("Node is the Head");
      console.log(this.head);
    }
    else{
      let prevNode = this.head;
      let nextNode = prevNode.next;
      // The first case is covered by above else if condition
      while(nextNode !== null){
        count++;
        if(nextNode.value === node){
          console.log("Found Element at position "+count);
          console.log("Node is "+JSON.stringify(nextNode));
          found =1;
        }
        prevNode = nextNode;
        nextNode = nextNode.next;
      }
      if(found === 0){
        console.log("Node not found");
      }
    }
  }

  reverse(){
    let current=this.head;
    let prev = null;
    let next = null;
    while(current !== null){
      next = current.next;
      current.next = prev;
      prev = current;
      current = next;
    }
    this.head = prev;
  }
}

function traverseAndPrint(list){
  //Traverse Linkedlist and print
  currentNode = list.head;
  do{
    process.stdout.write(currentNode.value+"->");
    next = currentNode.next;
    currentNode = next;

  }
  while(next.next !=null);

  //Print the last node
  console.log(currentNode.value);
}

// Play: create a list
const list = new Linkedlist("1");

// Add one element
list.addToHead("2");

// Add two more elements at head
list.addToHead("3")
    .addToHead("4");

traverseAndPrint(list);

console.log("Remove at head");
list.removeFromHead();
traverseAndPrint(list);

console.log("Add to List");
list.addToList("4");
list.addToList("5");
traverseAndPrint(list);

console.log("Remove from List");
list.removeANode("2");
traverseAndPrint(list);
list.findANode("3");
list.findANode("4");
list.findANode("2");

traverseAndPrint(list);
list.reverse();
traverseAndPrint(list);
